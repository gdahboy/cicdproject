package tn.esprit.spring.Timesheet;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import tn.esprit.spring.entities.*;
import tn.esprit.spring.repository.*;
import tn.esprit.spring.services.TimesheetServiceImpl;
import org.junit.rules.ExpectedException;
import static org.junit.jupiter.api.Assertions.assertThrows;


import java.util.*;

import static org.junit.Assert.*;



@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TimesheetTest {

    @Autowired
    TimesheetServiceImpl timesheetService ;
    @Autowired
    TimesheetRepository timesheetRepository;
    @Autowired
    MissionRepository missionRepository ;
    @Autowired
    EmployeRepository employeRepository ;
    @Autowired
    DepartementRepository departementRepository ;
    @Autowired
    EntrepriseRepository entrepriseRepository ;
    private static final Logger log = Logger.getLogger(TimesheetTest.class);




    @BeforeEach
    public  void  DataConfigueed() {
        log.info("Database connection ");
        Employe employe = new Employe( "Abderrahim", "Gdah" , "gdah" , "@esprit.tn" , true , Role.INGENIEUR) ;

        Mission mission = new Mission("Devops" , "infrastucture") ;


        Entreprise entreprise = new Entreprise("Esprit" , "12345" ) ;

        Departement departement = new Departement("Dev" ) ;


        mission.setDepartement(departement);
        departement.setEntreprise(entreprise);

        employeRepository.save(employe) ;
        log.info("save  employe ");
        entrepriseRepository.save(entreprise) ;
        log.info("save  entreprise ");
        departementRepository.save(departement) ;
        log.info("save  departemenr ");
        missionRepository.save(mission) ;
        log.info("save  mession ");

    }
    @AfterEach
    public  void destructData() {

        timesheetRepository.deleteAll();
        log.info("delete All timesheet");
        missionRepository.deleteAll();
        log.info("delete All mission");
        employeRepository.deleteAll();
        log.info("delete All employer");
        departementRepository.deleteAll();
        log.info("delete All departement");
        entrepriseRepository.deleteAll();
        log.info("delete All entreprise");
    }
    @Test
    public void testAddMission() {
        Employe employe = new Employe(1, "Abderrahim", "Gdah" , "gdah" , "@esprit.tn" , true , Role.INGENIEUR) ;
        Mission mission = new Mission("Devops" , "infrastucture") ;
        Timesheet timesheet = new Timesheet() ;
        timesheet.setMission(mission);
        assertTrue(timesheet.getMission().getName().equals(mission.getName()));

    }
    @Test
    public void testAddEmploye() {
        Employe employe = new Employe(1, "Abderrahim", "Gdah" , "gdah" , "@esprit.tn" , true , Role.INGENIEUR) ;
        Mission mission = new Mission("Devops" , "infrastucture") ;
        Timesheet timesheet = new Timesheet() ;
        timesheet.setEmploye(employe);
        assertTrue(timesheet.getEmploye().getNom().equals(employe.getNom()));

    }
    @Test
    public void testSavemission() {
        Mission mission = new Mission("Devops" , "infrastucture") ;
        Timesheet timesheet = new Timesheet() ;
        int idMission = timesheetService.ajouterMission(mission) ;
        assertTrue(idMission == mission.getId());

    }


    @Test
    public void testSaveTimesheet() {
        Employe employe = new Employe( "Abderrahim", "Gdah" , "gdah" , "@esprit.tn" , true , Role.INGENIEUR) ;
        Mission mission = new Mission("Devops" , "infrastucture" ) ;
        Departement departement = new Departement("Dev" ) ;
        Entreprise entreprise = new Entreprise("Esprit" , "12345" ) ;


        Timesheet timesheet = new Timesheet() ;
        TimesheetPK timesheetPK = new TimesheetPK() ;
        timesheetPK.setDateDebut(new Date(12, 10 , 2018));
        timesheetPK.setDateFin(new Date(10 , 10 , 2020));

        timesheet.setEmploye(employe);
        timesheet.setMission(mission);
        timesheet.setTimesheetPK(timesheetPK);
        timesheet.setValide(true);
       assertNotNull(timesheet.getTimesheetPK());

    }
    @Test
    public void saveTimesheet() {
        Employe employe = new Employe( "Abderrahim", "Gdah" , "gdah" , "@esprit.tn" , true , Role.INGENIEUR) ;
        Mission mission = new Mission("Devops" , "infrastucture" ) ;
        Departement departement = new Departement("Dev" ) ;
        Entreprise entreprise = new Entreprise("Esprit" , "12345" ) ;
        departement.setEntreprise(entreprise);
        List<Departement> list = new List<Departement>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Departement> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Departement departement) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Departement> c) {
                return false;
            }

            @Override
            public boolean addAll(int index, Collection<? extends Departement> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }

            @Override
            public Departement get(int index) {
                return null;
            }

            @Override
            public Departement set(int index, Departement element) {
                return null;
            }

            @Override
            public void add(int index, Departement element) {

            }

            @Override
            public Departement remove(int index) {
                return null;
            }

            @Override
            public int indexOf(Object o) {
                return 0;
            }

            @Override
            public int lastIndexOf(Object o) {
                return 0;
            }

            @Override
            public ListIterator<Departement> listIterator() {
                return null;
            }

            @Override
            public ListIterator<Departement> listIterator(int index) {
                return null;
            }

            @Override
            public List<Departement> subList(int fromIndex, int toIndex) {
                return null;
            }
        } ;
        list.add(departement) ;
        employe.setDepartements(list);
        mission.setDepartement(departement);

        Timesheet timesheet = new Timesheet() ;
        TimesheetPK timesheetPK = new TimesheetPK() ;
        timesheetPK.setDateDebut(new Date(12, 10 , 2018));
        timesheetPK.setDateFin(new Date(10 , 10 , 2020));

        timesheet.setEmploye(employe);
        timesheet.setMission(mission);
        timesheet.setTimesheetPK(timesheetPK);
        timesheet.setValide(true);
        //assertNotNull(timesheetRepository.save(timesheet));

    }




}
